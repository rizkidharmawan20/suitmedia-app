package com.example.suitmediaapp.modul.screen2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.suitmediaapp.R;
import com.example.suitmediaapp.component.PreferenceManager;
import com.example.suitmediaapp.modul.screen3.Screen3Activity;
import com.example.suitmediaapp.modul.screen4.Screen4Activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Screen2Activity extends AppCompatActivity {

    @BindView(R.id.tv_profile_name)
    TextView tv_profile_name;
    @BindView(R.id.btn_choose_event)
    Button btn_choose_event;
    @BindView(R.id.btn_choose_guest)
    Button btn_choose_guest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);
        ButterKnife.bind(this);

        initComponent();
    }

    private void initComponent() {
        if (!PreferenceManager.getScreen1Name().equals("")) {
            String[] separated = PreferenceManager.getScreen1Name().split(" ");
            String profileName = separated[0];
            tv_profile_name.setText(profileName);
        }

        String eventName = getIntent().getStringExtra("event_name");
        if (eventName!=null && !eventName.equals("")) {
            PreferenceManager.setChooseEvent(eventName);
            btn_choose_event.setText(eventName);
        }
        if (!PreferenceManager.getChooseEvent().equals("")) {
            btn_choose_event.setText(PreferenceManager.getChooseEvent());
        }

        String fullname = getIntent().getStringExtra("fullname");
        if (fullname!=null && !fullname.equals("")) {
            PreferenceManager.setChooseGuest(fullname);
            btn_choose_guest.setText(fullname);
        }
        if (!PreferenceManager.getChooseGuest().equals("")) {
            btn_choose_guest.setText(PreferenceManager.getChooseGuest());
        }
    }

    @OnClick(R.id.btn_choose_event)
    void onClickChooseEvent(){
        startActivity(new Intent(this, Screen3Activity.class));
    }

    @OnClick(R.id.btn_choose_guest)
    void onClickChooseGuest(){
        startActivity(new Intent(this, Screen4Activity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PreferenceManager.setChooseEvent("");
        PreferenceManager.setChooseGuest("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.setChooseEvent("");
        PreferenceManager.setChooseGuest("");
    }
}