package com.example.suitmediaapp.modul.screen3;

import com.example.suitmediaapp.component.PreferenceManager;
import com.example.suitmediaapp.component.network.NetworkService;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.component.network.response.ApiResponse;

import java.util.List;

import retrofit2.Call;

public class Screen3Interactor {
    private NetworkService mService;

    public Screen3Interactor(NetworkService service) {
        mService = service;
    }

    public Call<ApiResponse<List<Event>>> getListEvent(int page, int per_page) {
        return mService.getListEvent(page, per_page);
    }
}
