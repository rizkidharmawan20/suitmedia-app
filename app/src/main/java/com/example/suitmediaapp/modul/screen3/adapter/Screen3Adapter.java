package com.example.suitmediaapp.modul.screen3.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.suitmediaapp.R;
import com.example.suitmediaapp.component.PreferenceManager;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.modul.screen2.Screen2Activity;

import java.util.List;

public class Screen3Adapter extends RecyclerView.Adapter<Screen3Adapter.ViewHolder> {
    private List<Event> userList;
    private Activity activity;

    public Screen3Adapter(List<Event> userList, Activity activity){
        this.userList = userList;
        this.activity = activity;
        new PreferenceManager(activity);
    }

    @NonNull
    @Override
    public Screen3Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_events, parent, false);

        return new Screen3Adapter.ViewHolder(v);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull Screen3Adapter.ViewHolder holder, int position){
        try {
            final Event event = userList.get(position);
            holder.tv_title.setText(event.getName().toUpperCase());
            holder.layout.setOnClickListener(view -> {
                Intent intent = new Intent(activity, Screen2Activity.class);
                intent.putExtra("event_name", event.getName());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
                activity.finish();
            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount(){
        return userList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        TextView tv_body;
        TextView tv_date;
        TextView tv_time;
        ImageView img_events;
        CardView layout;

        ViewHolder(View v){
            super(v);

            tv_title = v.findViewById(R.id.tv_title);
            tv_body = v.findViewById(R.id.tv_body);
            tv_date = v.findViewById(R.id.tv_date);
            tv_time = v.findViewById(R.id.tv_time);
            img_events = v.findViewById(R.id.img_events);
            layout = v.findViewById(R.id.layout);
        }
    }
}
