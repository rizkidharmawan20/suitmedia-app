package com.example.suitmediaapp.modul.screen3;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.suitmediaapp.R;
import com.example.suitmediaapp.component.dialog.CustomProgressBar;
import com.example.suitmediaapp.component.interfacecom.CommonInterface;
import com.example.suitmediaapp.component.network.NetworkManager;
import com.example.suitmediaapp.component.network.NetworkService;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.util.GPSTracker;
import com.example.suitmediaapp.component.util.MethodUtil;
import com.example.suitmediaapp.modul.screen3.adapter.Screen3Adapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Screen3Activity extends FragmentActivity implements OnMapReadyCallback, CommonInterface, Screen3View {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1111;
    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.hometoolbar_title)
    TextView hometoolbar_title;
    @BindView(R.id.img_search)
    ImageView img_search;
    @BindView(R.id.img_maps)
    ImageView img_maps;
    @BindView(R.id.layout_maps)
    RelativeLayout layout_maps;
    @BindView(R.id.layout_card)
    CardView layout_card;

    private CustomProgressBar progressBar = new CustomProgressBar();
    private Screen3Presenter screen3Presenter;
    private GoogleMap mMap;
    private GPSTracker gpsTracker;
    private int page = 1;
    private int per_page = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen3);
        ButterKnife.bind(this);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

        screen3Presenter = new Screen3Presenter(this, this);
        initComponent();
    }

    @SuppressLint("SetTextI18n")
    private void initComponent() {
        hometoolbar_title.setVisibility(View.VISIBLE);
        hometoolbar_title.setText("Events");
        img_search.setVisibility(View.VISIBLE);
        img_maps.setVisibility(View.VISIBLE);
        pullToRefresh.setOnRefreshListener(() -> screen3Presenter.getListEvent(page, per_page));
        screen3Presenter.getListEvent(page, per_page);
        layout_maps.setVisibility(View.GONE);
    }

    @OnClick(R.id.img_maps)
    void onClickMaps(){
        if (layout_maps.getVisibility() == View.GONE) {
            layout_maps.setVisibility(View.VISIBLE);
            pullToRefresh.setVisibility(View.GONE);
            img_maps.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_format_list_bulleted_24));

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            assert mapFragment != null;
            mapFragment.getMapAsync(this);
            Activity activity = this;
            gpsTracker = new GPSTracker(activity);

        } else {
            layout_maps.setVisibility(View.GONE);
            pullToRefresh.setVisibility(View.VISIBLE);
            img_maps.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_map_24));
        }
    }

    @OnClick(R.id.hometoolbar_back)
    void onClickBack(){
        onBackPressed();
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        tvNoData.setVisibility(View.VISIBLE);
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_error_login);
    }

    @Override
    public void onSuccessGetData(List<Event> userList) {
        if (pullToRefresh.isRefreshing()) pullToRefresh.setRefreshing(false);
        if (userList.size()>0){
            Screen3Adapter adapter = new Screen3Adapter(userList, this);
            recyclerView.setAdapter(adapter);
            tvNoData.setVisibility(View.GONE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng lng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        mMap.addMarker(new MarkerOptions().position(lng).title("Posisi Anda"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()), 16.0f));

        googleMap.setOnMapClickListener(latLng -> {

            // Creating a marker
            MarkerOptions markerOptions = new MarkerOptions();

            // Setting the position for the marker
            markerOptions.position(latLng);

            // Setting the title for the marker.
            // This will be displayed on taping the marker
            markerOptions.title(latLng.latitude + " : " + latLng.longitude);

            // Clears the previously touched position
            googleMap.clear();

            // Animating to the touched position
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

            // Placing a marker on the touched position
            googleMap.addMarker(markerOptions);

            layout_card.setVisibility(View.VISIBLE);
        });
    }
}