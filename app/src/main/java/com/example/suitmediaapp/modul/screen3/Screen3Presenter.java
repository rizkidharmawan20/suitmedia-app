package com.example.suitmediaapp.modul.screen3;

import com.example.suitmediaapp.component.interfacecom.CommonInterface;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.component.network.response.ApiResponse;
import com.example.suitmediaapp.component.util.MethodUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Screen3Presenter {
    private CommonInterface cInterface;
    private Screen3View mView;
    private Screen3Interactor mInteractor;

    public Screen3Presenter(CommonInterface cInterface, Screen3View view) {
        mView = view;
        this.cInterface = cInterface;
        mInteractor = new Screen3Interactor(this.cInterface.getService());
    }

    public void getListEvent(int page, int per_page) {
        cInterface.showProgressLoading();
        mInteractor.getListEvent(page, per_page).enqueue(new Callback<ApiResponse<List<Event>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Event>>> call, Response<ApiResponse<List<Event>>> response) {
                cInterface.hideProgresLoading();
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        mView.onSuccessGetData(response.body().getData());
                    } else {
                        assert response.errorBody() != null;
                        String msg = MethodUtil.getErrorResponse(response.errorBody().string());
                        cInterface.onFailureRequest(msg);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Event>>> call, Throwable t) {
                t.printStackTrace();
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(t.getMessage());
            }
        });
    }
}
