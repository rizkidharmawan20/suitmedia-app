package com.example.suitmediaapp.modul.screen4;

import com.example.suitmediaapp.component.network.entities.User;

import java.util.List;

public interface Screen4View {
    void onSuccessGetData(List<User> userList);
    void onSuccessLoadNextDataFromApi(List<User> userList);
}