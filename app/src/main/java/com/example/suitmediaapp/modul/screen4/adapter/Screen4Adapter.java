package com.example.suitmediaapp.modul.screen4.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.suitmediaapp.R;
import com.example.suitmediaapp.component.PreferenceManager;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.modul.screen2.Screen2Activity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

public class Screen4Adapter extends RecyclerView.Adapter<Screen4Adapter.ViewHolder> {
    private List<User> userList;
    private Activity activity;

    public Screen4Adapter(List<User> userList, Activity activity){
        this.userList = userList;
        this.activity = activity;
        new PreferenceManager(activity);
    }

    @NonNull
    @Override
    public Screen4Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_guest, parent, false);

        return new Screen4Adapter.ViewHolder(v);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull Screen4Adapter.ViewHolder holder, int position){
        try {
            final User user = userList.get(position);
            final String fullname = user.first_name + " " + user.last_name;
            final String image = user.avatar;

            holder.tv_name.setText(fullname);
            Glide.with(activity)
                    .load(image)
                    .into(holder.img_events);
            holder.layout.setOnClickListener(view -> {
                Intent intent = new Intent(activity, Screen2Activity.class);
                intent.putExtra("fullname", fullname);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
                activity.finish();
            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount(){
        return userList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        CircularImageView img_events;
        LinearLayout layout;

        ViewHolder(View v){
            super(v);

            tv_name = v.findViewById(R.id.tv_name);
            img_events = v.findViewById(R.id.img_events);
            layout = v.findViewById(R.id.layout);
        }
    }
}
