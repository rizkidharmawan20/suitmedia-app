package com.example.suitmediaapp.modul.screen3;

import com.example.suitmediaapp.component.network.entities.Event;

import java.util.List;

public interface Screen3View {
    void onSuccessGetData(List<Event> userList);
}
