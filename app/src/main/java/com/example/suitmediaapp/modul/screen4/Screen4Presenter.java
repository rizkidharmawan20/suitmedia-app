package com.example.suitmediaapp.modul.screen4;

import com.example.suitmediaapp.component.interfacecom.CommonInterface;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.component.network.response.ApiResponse;
import com.example.suitmediaapp.component.util.MethodUtil;
import com.example.suitmediaapp.modul.screen3.Screen3Interactor;
import com.example.suitmediaapp.modul.screen3.Screen3View;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Screen4Presenter {
    private CommonInterface cInterface;
    private Screen4View mView;
    private Screen4Interactor mInteractor;

    public Screen4Presenter(CommonInterface cInterface, Screen4View view) {
        mView = view;
        this.cInterface = cInterface;
        mInteractor = new Screen4Interactor(this.cInterface.getService());
    }

    public void getListUser(int page, int per_page) {
        cInterface.showProgressLoading();
        mInteractor.getListUser(page, per_page).enqueue(new Callback<ApiResponse<List<User>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<User>>> call, Response<ApiResponse<List<User>>> response) {
                cInterface.hideProgresLoading();
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        mView.onSuccessGetData(response.body().getData());
                    } else {
                        assert response.errorBody() != null;
                        String msg = MethodUtil.getErrorResponse(response.errorBody().string());
                        cInterface.onFailureRequest(msg);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<User>>> call, Throwable t) {
                t.printStackTrace();
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(t.getMessage());
            }
        });
    }

    public void loadNextDataFromApi(int page, int per_page) {
        cInterface.showProgressLoading();
        mInteractor.getListUser(page, per_page).enqueue(new Callback<ApiResponse<List<User>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<User>>> call, Response<ApiResponse<List<User>>> response) {
                cInterface.hideProgresLoading();
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        mView.onSuccessLoadNextDataFromApi(response.body().getData());
                    } else {
                        assert response.errorBody() != null;
                        String msg = MethodUtil.getErrorResponse(response.errorBody().string());
                        cInterface.onFailureRequest(msg);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<User>>> call, Throwable t) {
                t.printStackTrace();
                cInterface.hideProgresLoading();
                cInterface.onFailureRequest(t.getMessage());
            }
        });
    }
}
