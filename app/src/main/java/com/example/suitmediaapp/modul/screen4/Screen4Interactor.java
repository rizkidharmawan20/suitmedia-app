package com.example.suitmediaapp.modul.screen4;

import com.example.suitmediaapp.component.network.NetworkService;
import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.component.network.response.ApiResponse;

import java.util.List;

import retrofit2.Call;

public class Screen4Interactor {
    private NetworkService mService;

    public Screen4Interactor(NetworkService service) {
        mService = service;
    }

    public Call<ApiResponse<List<User>>> getListUser(int page, int per_page) {
        return mService.getListUser(page, per_page);
    }
}
