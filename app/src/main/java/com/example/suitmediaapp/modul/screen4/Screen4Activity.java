package com.example.suitmediaapp.modul.screen4;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.suitmediaapp.R;
import com.example.suitmediaapp.component.PreferenceManager;
import com.example.suitmediaapp.component.dialog.CustomProgressBar;
import com.example.suitmediaapp.component.interfacecom.CommonInterface;
import com.example.suitmediaapp.component.network.NetworkManager;
import com.example.suitmediaapp.component.network.NetworkService;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.component.util.EndlessRecyclerViewScrollListener;
import com.example.suitmediaapp.component.util.MethodUtil;
import com.example.suitmediaapp.modul.screen4.adapter.Screen4Adapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Screen4Activity extends AppCompatActivity implements CommonInterface, Screen4View {

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.hometoolbar_title)
    TextView hometoolbar_title;
    @BindView(R.id.img_search)
    ImageView img_search;
    @BindView(R.id.img_maps)
    ImageView img_maps;

    private CustomProgressBar progressBar = new CustomProgressBar();
    private Screen4Presenter screen4Presenter;
    private int page = 1;
    private int per_page = 10;
    private Screen4Adapter adapter;
    private List<User> mUserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen4);
        ButterKnife.bind(this);

        screen4Presenter = new Screen4Presenter(this, this);
        initComponent();
    }

    @SuppressLint("SetTextI18n")
    private void initComponent() {
        hometoolbar_title.setVisibility(View.VISIBLE);
        hometoolbar_title.setText("Guests");
        img_search.setVisibility(View.GONE);
        img_maps.setVisibility(View.GONE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.d("TAG", "onLoadMore: " + page + " - " + totalItemsCount);
                loadNextDataFromApi(page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        pullToRefresh.setColorSchemeResources(
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_light);

        if (MethodUtil.isInternetAvailable()) {
            pullToRefresh.setOnRefreshListener(() -> screen4Presenter.getListUser(page, per_page));
            screen4Presenter.getListUser(page, per_page);
        } else {
            mUserList = PreferenceManager.getListUser();
            adapter = new Screen4Adapter(mUserList, this);
            if (mUserList.size() == 0) MethodUtil.showCustomToast(this, "Please check your internet connection", R.drawable.ic_error_login);
        }
    }

    private void loadNextDataFromApi(int page) {
        screen4Presenter.loadNextDataFromApi(page, per_page);
    }

    @OnClick(R.id.hometoolbar_back)
    void onClickBack(){
        onBackPressed();
    }

    @Override
    public void showProgressLoading() {
        progressBar.show(this, "", false, null);
    }

    @Override
    public void hideProgresLoading() {
        progressBar.getDialog().dismiss();
    }

    @Override
    public NetworkService getService() {
        return NetworkManager.getInstance();
    }

    @Override
    public void onFailureRequest(String msg) {
        tvNoData.setVisibility(View.VISIBLE);
        MethodUtil.showCustomToast(this, msg, R.drawable.ic_error_login);
    }

    @Override
    public void onSuccessGetData(List<User> userList) {
        if (pullToRefresh.isRefreshing()) pullToRefresh.setRefreshing(false);
        mUserList = userList;
        PreferenceManager.setListUser(mUserList);
        if (userList.size()>0){
            adapter = new Screen4Adapter(mUserList, this);
            recyclerView.setAdapter(adapter);
            tvNoData.setVisibility(View.GONE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccessLoadNextDataFromApi(List<User> userList) {
        if (pullToRefresh.isRefreshing()) pullToRefresh.setRefreshing(false);
        mUserList.addAll(userList);
        adapter.notifyDataSetChanged();
        PreferenceManager.setListUser(mUserList);
    }
}