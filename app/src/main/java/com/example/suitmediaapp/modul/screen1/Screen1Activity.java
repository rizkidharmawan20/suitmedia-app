package com.example.suitmediaapp.modul.screen1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.example.suitmediaapp.R;
import com.example.suitmediaapp.component.PreferenceManager;
import com.example.suitmediaapp.component.dialog.CustomProgressBar;
import com.example.suitmediaapp.component.interfacecom.CommonInterface;
import com.example.suitmediaapp.component.network.NetworkManager;
import com.example.suitmediaapp.component.network.NetworkService;
import com.example.suitmediaapp.component.util.MethodUtil;
import com.example.suitmediaapp.modul.screen2.Screen2Activity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class Screen1Activity extends AppCompatActivity {

    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_palindrome)
    EditText et_palindrome;
    @BindView(R.id.img_profile)
    CircularImageView img_profile;

    private static final int CAMERA_REQUEST_CODE = 1;
    private Bitmap selectedImage;
    private ArrayList<File> photos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_1);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.img_profile)
    void onClickImage() {
        selectImage();
    }

    @OnClick(R.id.btn_next)
    void onClickLoginBtn() {
        if (TextUtils.isEmpty(et_name.getText().toString())) {
            Toast.makeText(getApplication(), "Please type name", Toast.LENGTH_SHORT).show();
            et_name.setError("Please type name");
        } else if (TextUtils.isEmpty(et_palindrome.getText().toString())) {
            Toast.makeText(getApplication(), "Please type palindrome", Toast.LENGTH_SHORT).show();
            et_palindrome.setError("Please type palindrome");
        } else if (!MethodUtil.sentencePalindrome(et_palindrome.getText().toString())) {
            Toast.makeText(this, "Sentence is not palindrome", Toast.LENGTH_SHORT).show();
            et_palindrome.setError("Sentence is not palindrome");
        } else {
            PreferenceManager.saveScreen1Name(et_name.getText().toString());
            PreferenceManager.saveScreen1Image(selectedImage);
            startActivity(new Intent(this, Screen2Activity.class));
        }
    }
    
    @OnClick(R.id.btn_check)
    void onClickBtnCheck() {
        if (TextUtils.isEmpty(et_palindrome.getText().toString())) {
            Toast.makeText(getApplication(), "Please type palindrome", Toast.LENGTH_SHORT).show();
            et_palindrome.setError("Please type palindrome");
            return;
        }

        checkPalindrome(et_palindrome.getText().toString());
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
                } else {
                    EasyImage.openCameraForImage(this, 1111);
                }
            } else if (options[item].equals("Choose From Gallery")) {
                EasyImage.openGallery(this, 2222);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void onPhotosReturned(List<File> returnedPhotos) {
        photos.addAll(returnedPhotos);
        if (photos.size()>0){
            Bitmap bitmap = BitmapFactory.decodeFile(photos.get(0).getAbsolutePath());
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(photos.get(0).getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            assert ei != null;
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
            selectedImage = rotatedBitmap;
            Glide.with(this).load(photos.get(0)).into(img_profile);
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public void checkPalindrome(String text) {
        if (MethodUtil.sentencePalindrome(text)) {
            Toast.makeText(this, "Sentence is palindrome", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Sentence is not palindrome", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                onPhotosReturned(imageFiles);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA_IMAGE) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(Screen1Activity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }
}