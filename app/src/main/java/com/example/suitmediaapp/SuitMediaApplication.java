package com.example.suitmediaapp;

import androidx.multidex.MultiDexApplication;

import com.example.suitmediaapp.component.PreferenceManager;

public class SuitMediaApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        initPrefrences();
    }

    private void initPrefrences() {
        new PreferenceManager(this);
    }
}
