package com.example.suitmediaapp.component.interfacecom;

import com.example.suitmediaapp.component.network.NetworkService;

public interface CommonInterface {
    void showProgressLoading();

    void hideProgresLoading();

    NetworkService getService();

    void onFailureRequest(String msg);
}
