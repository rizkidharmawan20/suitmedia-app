package com.example.suitmediaapp.component.network;

import com.example.suitmediaapp.component.network.entities.Event;
import com.example.suitmediaapp.component.network.entities.User;
import com.example.suitmediaapp.component.network.response.ApiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkService {

    String BASE_URL = "https://reqres.in/api/";

    @GET("users")
    Call<ApiResponse<List<User>>> getListUser(@Query("page") int page,
                                              @Query("per_page") int per_page);

    @GET("events")
    Call<ApiResponse<List<Event>>> getListEvent(@Query("page") int page,
                                               @Query("per_page") int per_page);

}


