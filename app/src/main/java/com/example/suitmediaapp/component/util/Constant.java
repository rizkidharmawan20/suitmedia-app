package com.example.suitmediaapp.component.util;

public class Constant {
    public static final int INTERNET_DATA = 0;

    public static final int PULSA_HANDPHONE = 1;

    public static final int PASCABAYAR = 2;

    public static final int PDAM = 3;

    public static final int LISTRIK = 4;

    public static final int BPJS_KESEHATAN = 5;

    public static final int BPJS_KETENAGAKERJAAN = 6;

    public static final int TV_INTERNET = 7;

    public static final int CICILAN = 8;

    public static final int ASURANSI = 9;

    public static final int BAZNAS = 10;

    public static final int INTERNET = 11;

    public static final String EXPIRED_SESSION = "User tidak dikenali.";

    public static final String EXPIRED_ACCESS_TOKEN = "AccessToken is already expired.";

    public static final String POSITION = "position";

    public static final String TRANSACTION = "transaction";

    public static final String ERROR_ALREADY_REGISTERED = "Mobile phone number is already registered";

    public static final String TOPUP_STATUS_WAITING = "3";

    public static final String TOPUP_STATUS_ON_PROCESS = "4";

    public static final String TOPUP_STATUS_SUCCESS = "5";

    public static final String TOPUP_STATUS_REJECT = "6";

    public static final String DO_PRINT = "1";

    public static final String SERVICE_PULSA = "0";
    public static final String SERVICE_PAKET_DATA = "1";
    public static final String SERVICE_PDAM = "2";
    public static final String SERVICE_PLN = "3";
    public static final String SERVICE_TELKOM = "4";
    public static final String SERVICE_MULTIFINANCE = "5";
    public static final String SERVICE_TV = "6";
    public static final String SERVICE_ASURANSI = "7";
    public static final String SERVICE_KARTU_KREDIT = "8";
    public static final String SERVICE_ISP = "9";
    public static final String SERVICE_BPJS = "11";

    public static final String SERVICE_PURCHASE = "0";
    public static final String SERVICE_JIWASRAYA = "161";
    public static final String SERVICE_PAYMENT = "1";

    public static final int TYPE_REFERRAL = 1;
    public static final int TYPE_TRANSACTION = 2;
    public static final int TYPE_TOPUP = 3;
    public static final int TYPE_REDEEM = 4;

    public static final String CHAT_SERVER_URL = "128.199.223.237";
    public static final String BELUM_SETTLE = "0";
    public static final String SUDAH_SETTLE = "1";
    public static final String UID = "UID";


//SETUP PRINTER BT
    public static final String stripe_printer_42 = "------------------------------------------";
    public static final String stripe_printer_42_with_padding_left = "    ------------------------------------";
    public static final int printer_length_42 = 42;

    public static final String stripe_printer_32 = "--------------------------------";
    public static final String stripe_printer_32_with_padding_left = "    --------------------------";
    public static final int printer_length_32 = 32;

    public static final String stripe_printer = stripe_printer_42;
    public static final String stripe_printer_with_padding_left = stripe_printer_42_with_padding_left;
    public static final int printer_length = printer_length_42;

    public static final String PUBLISH = "publish";
    public static final String OUT_OF_STOCK = "outofstock";
    public static final String STATUS_VISIBLE = "visible";

    public static final String ON_HOLD_STATUS = "on-hold";
    public static final String PROCCESSING_STATUS = "processing";
    public static final String COMPLETED_STATUS = "completed";
    public static final String PAYMENT_METHOD_COD = "cod";
    public static final String PAYMENT_METHOD_TRANFSER_BANK = "bacs";
    public static final String PAYMENT_METHOD_TRANFSER_BANK_TITLE = "Transfer Bank";
    public static final String PAYMENT_METHOD_QRIS_TITLE = "QRIS";
    public static final String PAYMENT_METHOD_COD_TITLE = "Bayar di Tempat (COD)";

    public static final String SHIPPING_METHOD_ID = "free_shipping";
    public static final String SHIPPING_METHOD_TITLE = "Gratis Ongkir";
    public static final String SHIPPING_TOTAL = "0";

    public static final String JSON = "[{\n" +
            "  \"name\": \"Totole Kaldu Ayam\",\n" +
            "  \"mass_unit\": \"Gram\",\n" +
            "  \"qty\": 200,\n" +
            "  \"id\": 2135\n" +
            "},\n" +
            "{\n" +
            "  \"name\": \"Ikan Tenggiri\",\n" +
            "  \"mass_unit\": \"Kg\",\n" +
            "  \"qty\": 2,\n" +
            "  \"id\": 2136\n" +
            "},\n" +
            "{\n" +
            "  \"name\": \"Bawang Putih\",\n" +
            "  \"mass_unit\": \"Gram\",\n" +
            "  \"qty\": 100,\n" +
            "  \"id\": 2137\n" +
            "},\n" +
            "{\n" +
            "  \"name\": \"Bawang Merah\",\n" +
            "  \"mass_unit\": \"Gram\",\n" +
            "  \"qty\": 100,\n" +
            "  \"id\": 2138\n" +
            "},\n" +
            "{\n" +
            "  \"name\": \"Cabai Merah\",\n" +
            "  \"mass_unit\": \"Gram\",\n" +
            "  \"qty\": 350,\n" +
            "  \"id\": 2139\n" +
            "}]";

    public static final String consumer_key_seladasegarbandung = "ck_d04d96b66ab13a3d4cc2388e88966b6ea74ac9d5";
    public static final String consumer_secret_seladasegarbandung = "cs_6601bed02e3e4ca88cd9beb6b397d758620b7d60";

    //NURUL FIKRI
    public static final String PARTNER_ID_NURUL_FIKRI = "1a1Ed013-fac4-42de-a959-a4bf06b8e72d";

    public static String getConsumerKey() {
        return "ck_d04d96b66ab13a3d4cc2388e88966b6ea74ac9d5";
    }

    public static String getConsumerSecret() {
        return "cs_6601bed02e3e4ca88cd9beb6b397d758620b7d60";
    }
}
