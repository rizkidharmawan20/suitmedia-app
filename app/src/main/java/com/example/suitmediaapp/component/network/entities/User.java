package com.example.suitmediaapp.component.network.entities;

public class User extends BaseEntity {
    public String first_name;
    public String last_name;
    public String email;
    public String avatar;
}
