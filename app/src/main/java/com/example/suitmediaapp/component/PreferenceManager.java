package com.example.suitmediaapp.component;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.suitmediaapp.component.network.entities.User;
import com.orhanobut.hawk.Hawk;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PreferenceManager {

    private static final String SESSION_TOKEN = "sessionToken";
    private static final String SCREEN_1_NAME = "screen1Name";
    private static final String SCREEN_1_IMAGE = "screen1Image";
    private static final String LIST_GUEST = "listGuest";
    private static final String CHOOSE_EVENT = "chooseEvent";
    private static final String CHOOSE_GUEST = "chooseGuest";


    private static final String USER = "user";

    private static Context ctx;
    private static PreferenceManager mInstance;

    public PreferenceManager(Context context) {
        Hawk.init(context).build();
    }
    public static synchronized PreferenceManager getInstance(Context context){
        if (mInstance == null)
            mInstance = new PreferenceManager(context);
        return mInstance;
    }



    public static String getChooseEvent() {
        return Hawk.get(CHOOSE_EVENT, "");
    }

    public static void setChooseEvent(String chooseEvent) {
        Hawk.put(CHOOSE_EVENT, chooseEvent);
    }

    public static String getChooseGuest() {
        return Hawk.get(CHOOSE_GUEST, "");
    }

    public static void setChooseGuest(String chooseGuest) {
        Hawk.put(CHOOSE_GUEST, chooseGuest);
    }

    public static List<User> getListUser(){
        return Hawk.get(LIST_GUEST, new ArrayList<>());
    }

    public static void setListUser(List<User> listUser) {
        Hawk.put(LIST_GUEST, listUser);
    }

    public static String getSessionToken() {
        return Hawk.get(SESSION_TOKEN, "");
    }


    public static void setSessionToken(String token) {
        Hawk.put(SESSION_TOKEN, token);
    }

    public static void saveUser(User user) {
        Hawk.put(USER, user);
    }

    public static User getUser() {
        return Hawk.get(USER);
    }

    public static void saveScreen1Name(String name) {
        Hawk.put(SCREEN_1_NAME, name);
    }

    public static String getScreen1Name() {
        return Hawk.get(SCREEN_1_NAME, "");
    }

    public static void saveScreen1Image(Bitmap image) {
        Hawk.put(SCREEN_1_IMAGE, image);
    }

    public static Bitmap getScreen1Image() {
        return Hawk.get(SCREEN_1_IMAGE);
    }
}
